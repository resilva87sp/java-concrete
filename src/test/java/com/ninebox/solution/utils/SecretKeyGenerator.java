package com.ninebox.solution.utils;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.junit.Ignore;

import javax.crypto.SecretKey;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;

@Ignore
public class SecretKeyGenerator {

    public static void main (String[] args) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        SecureRandom sr = new SecureRandom();
        SecretKey key = MacProvider.generateKey(SignatureAlgorithm.HS512, sr);
        KeyStore ks = KeyStore.getInstance("JCEKS");
        FileOutputStream fos = new FileOutputStream("/opt/keystore");
        char[] password = "teste123".toCharArray();
        ks.load(null, password);
        KeyStore.SecretKeyEntry entry = new KeyStore.SecretKeyEntry(key);
        KeyStore.ProtectionParameter protection = new KeyStore.PasswordProtection(password);
        ks.setEntry("jwt-key", entry, protection);
        ks.store(fos, password);
    }

}
