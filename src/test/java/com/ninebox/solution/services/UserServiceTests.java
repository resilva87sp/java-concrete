package com.ninebox.solution.services;

import com.ninebox.solution.entities.PhoneEntity;
import com.ninebox.solution.entities.UserEntity;
import com.ninebox.solution.pojo.LoginPojo;
import com.ninebox.solution.pojo.PhonePojo;
import com.ninebox.solution.pojo.UserPojo;
import com.ninebox.solution.repositories.PhoneRepository;
import com.ninebox.solution.repositories.UserRepository;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import javax.crypto.SecretKey;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    private static List<PhonePojo> MOCKED_PHONES;
    private static UserEntity MOCKED_USER_ENTITY;

    @Mock
    private KeystoreReader encryptionReader;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PhoneRepository phoneRepository;

    @Mock
    private Environment environment;

    private StrongPasswordEncryptor passwordEncryptor;

    @InjectMocks
    private UserService service;

    @BeforeClass
    public static void initClass () {
        PhonePojo phone1 = new PhonePojo();
        phone1.setDdd("11");
        phone1.setNumber("966668877");
        PhonePojo phone2 = new PhonePojo();
        phone2.setDdd("11");
        phone2.setNumber("966660055");
        MOCKED_PHONES = Arrays.asList(phone1, phone2);
        MOCKED_USER_ENTITY = new UserEntity();
        MOCKED_USER_ENTITY.setId(1l);
    }

    @Before
    public void initTests () {
        service.init();
        when(encryptionReader.getKey()).thenReturn(getKey());
        when(userRepository.save(any(UserEntity.class))).thenReturn(MOCKED_USER_ENTITY);
        when(phoneRepository.save(anyListOf(PhoneEntity.class))).thenReturn(null);
        when(environment.getProperty(anyString(), eq(Integer.class), anyInt())).thenReturn(5);
        passwordEncryptor = new StrongPasswordEncryptor();
    }

    @Test
    public void testCreate () {
        UserPojo aUser = new UserPojo();
        aUser.setName("Test");
        aUser.setEmail("test@test.org");
        aUser.setPassword("123456");
        aUser.setPhones(MOCKED_PHONES);
        UserPojo savedUser = service.create(aUser);
        assertThat(savedUser.getId(), equalTo(1l));
        verify(userRepository, only()).save(any(UserEntity.class));
        verify(phoneRepository, only()).save(anyListOf(PhoneEntity.class));
        assertThat(savedUser.getToken(), notNullValue());
    }

    @Test
    public void testSave () {
        UserPojo aUser = new UserPojo();
        aUser.setName("Test");
        aUser.setEmail("test@test.org");
        aUser.setPassword("123456");
        aUser.setPhones(MOCKED_PHONES);
        service.save(aUser);
        verify(userRepository, only()).save(any(UserEntity.class));
    }

    @Test
    public void testIsPasswordValidOK () {
        UserPojo aUser = new UserPojo();
        aUser.setPassword(passwordEncryptor.encryptPassword("123456"));
        LoginPojo login = new LoginPojo();
        login.setPassword("123456");
        boolean isPasswordValid = service.isPasswordValid(login, aUser);
        assertThat(isPasswordValid, is(true));
    }

    @Test
    public void testIsPasswordValidInvalid () {
        UserPojo aUser = new UserPojo();
        aUser.setPassword(passwordEncryptor.encryptPassword("987654"));
        LoginPojo login = new LoginPojo();
        login.setPassword("123456");
        boolean isPasswordValid = service.isPasswordValid(login, aUser);
        assertThat(isPasswordValid, is(false));
    }

    @Test
    public void testVerifySessionOK () {
        Date lastLogin = getDateMinusMinutes(2);
        UserPojo aUser = new UserPojo();
        aUser.setLastLoginAt(lastLogin);
        boolean isSessionOk = service.isSessionValid(aUser);
        assertThat(isSessionOk, is(true));
    }

    @Test
    public void testVerifySessionNullLastLogin () {
        UserPojo aUser = new UserPojo();
        aUser.setLastLoginAt(null);
        boolean isSessionOk = service.isSessionValid(aUser);
        assertThat(isSessionOk, is(false));
    }

    @Test
    public void testVerifySessionInvalidSession () {
        Date lastLogin = getDateMinusMinutes(10);
        UserPojo aUser = new UserPojo();
        aUser.setLastLoginAt(lastLogin);
        boolean isSessionOk = service.isSessionValid(aUser);
        assertThat(isSessionOk, is(false));
    }

    @Test
    public void testFindByEmail () {
        UserPojo savedUser = service.findByEmail("test@test.org");
        verify(userRepository, only()).findByEmail(anyString());
    }

    @Test
    public void testFindByToken () {
        UserPojo savedUser = service.findByToken("1620371y29831289316723712731g2y3g6ge6r1");
        verify(userRepository, only()).findByToken(anyString());
    }

    private SecretKey getKey () {
        SecureRandom sr = new SecureRandom();
        SecretKey key = MacProvider.generateKey(SignatureAlgorithm.HS512, sr);
        return key;
    }

    private Date getDateMinusMinutes (int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, -1 * minutes);
        return cal.getTime();
    }
}
