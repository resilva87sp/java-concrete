package com.ninebox.solution.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import java.security.Key;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class KeystoreReaderTests {

    @Mock
    private Environment env;

    @InjectMocks
    private KeystoreReader keystoreReader;

    @Before
    public void initTests () throws Exception {
        when(env.getProperty(anyString())).thenReturn("teste123");
        keystoreReader.init();
    }

    @Test
    public void testGetKey () {
        Key key = keystoreReader.getKey();
        assertThat(key, notNullValue());
    }

}
