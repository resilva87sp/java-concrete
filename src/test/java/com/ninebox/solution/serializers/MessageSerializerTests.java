package com.ninebox.solution.serializers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ninebox.solution.errors.Messages;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MessageSerializerTests {


    private MessagesSerializer serializer;

    @Before
    public void initTests () {
        serializer = new MessagesSerializer();
    }

    @Test
    public void testSerialize () throws IOException {
        String json = new ObjectMapper().writeValueAsString(Messages.EMAIL_ALREADY_EXISTS);
        assertThat(json, equalTo("{\"message\":\"Email already exists.\"}"));
    }

}
