package com.ninebox.solution.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ninebox.solution.Solution;
import com.ninebox.solution.errors.Messages;
import com.ninebox.solution.errors.RESTExceptionHandler;
import com.ninebox.solution.pojo.LoginPojo;
import com.ninebox.solution.pojo.PhonePojo;
import com.ninebox.solution.pojo.UserPojo;
import com.ninebox.solution.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Solution.class)
public class UserControllerTests {

    private UserController userController;

    private MockMvc mockMvc;

    private UserService userService;

    private UserPojo user1, user2;

    private ObjectMapper mapper;

    @Before
    public void initTests () {
        userController = new UserController();
        userService = mock(UserService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(userController)
                .setControllerAdvice(new RESTExceptionHandler())
                .build();
        ReflectionTestUtils.setField(userController, "service", userService);

        user1 = new UserPojo();
        user1.setId(1l);
        user1.setEmail("test1@test.org");
        user1.setPassword("test");
        user1.setName("Test1");
        user1.setPhones(Collections.<PhonePojo>emptyList());

        user2 = new UserPojo();
        user2.setId(2l);
        user2.setEmail("test2@test.org");
        user2.setPassword("test2");
        user2.setName("Test2");
        user2.setPhones(Collections.<PhonePojo>emptyList());

        mapper = new ObjectMapper();
    }


    @Test
    public void testCreateOK () throws Exception {
        when(userService.findByEmail(anyString())).thenReturn(null);
        when(userService.create(any(UserPojo.class))).thenReturn(user1);
        mockMvc.perform(
                post("/users/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(user1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"name\":\"Test1\",\"email\":\"test1@test.org\"," +
                        "\"password\":\"test\",\"phones\":[],\"createdAt\":null,\"modifiedAt\":null," +
                        "\"lastLoginAt\":null," +
                        "\"token\":null}"));
    }

    //@Test(expected = EmailAlreadyExistsException.class)
    @Test
    public void testCreateEmailAlreadyExists () throws Exception {
        when(userService.findByEmail(anyString())).thenReturn(user1);
        mockMvc.perform(
                post("/users/create").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(user1)))
                .andDo(print())
                .andExpect(status().isConflict())
                .andExpect(content().json("{\"message\":\"" + Messages.EMAIL_ALREADY_EXISTS.value() + "\"}"));
    }

    @Test
    public void testLoginOK () throws Exception {
        when(userService.findByEmail(anyString())).thenReturn(user1);
        when(userService.isPasswordValid(any(LoginPojo.class), any(UserPojo.class)))
                .thenReturn(true);
        when(userService.save(any(UserPojo.class))).thenReturn(user1);
        MvcResult mvcResult = mockMvc.perform(
                post("/users/login").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(user1)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
        String contentAsString = mvcResult.getResponse().getContentAsString();
        UserPojo loggedUser = mapper.readValue(contentAsString, UserPojo.class);
        assertThat(loggedUser.getLastLoginAt(), notNullValue());
    }

    @Test
    public void testLoginUserNotFound () throws Exception {
        when(userService.findByEmail(anyString())).thenReturn(null);
        mockMvc.perform(
                post("/users/login").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(user1)))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"message\":\"" + Messages.INVALID_LOGIN.value() + "\"}"));
    }

    @Test
    public void testLoginInvalidPassword () throws Exception {
        when(userService.findByEmail(anyString())).thenReturn(user1);
        when(userService.isPasswordValid(any(LoginPojo.class), any(UserPojo.class)))
                .thenReturn(false);
        mockMvc.perform(
                post("/users/login").contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(mapper.writeValueAsString(user1)))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().json("{\"message\":\"" + Messages.INVALID_LOGIN.value() + "\"}"));
    }

    @Test
    public void testProfileOK () throws Exception {
        when(userService.findByToken(anyString())).thenReturn(user1);
        when(userService.isSessionValid(any(UserPojo.class))).thenReturn(true);
        mockMvc.perform(
                get("/users/profile/1").header("X-Auth-Token", "1023123123y12g31926391g2371623"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{\"id\":1,\"name\":\"Test1\",\"email\":\"test1@test.org\"," +
                        "\"password\":\"test\",\"phones\":[],\"createdAt\":null,\"modifiedAt\":null," +
                        "\"lastLoginAt\":null," +
                        "\"token\":null}"));
    }

    @Test
    public void testProfileEmptyHeader () throws Exception {
        mockMvc.perform(
                get("/users/profile/1"))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"message\":\"" + Messages.UNAUTHORIZED_ACCESS.value() + "\"}"));

    }

    @Test
    public void testProfileUserNotFound () throws Exception {
        when(userService.findByToken(anyString())).thenReturn(null);
        mockMvc.perform(
                get("/users/profile/1").header("X-Auth-Token", "1023123123y12g31926391g2371623"))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"message\":\"" + Messages.UNAUTHORIZED_ACCESS.value() + "\"}"));
    }

    @Test
    public void testProfileInvalidId () throws Exception {
        when(userService.findByToken(anyString())).thenReturn(user2);
        mockMvc.perform(
                get("/users/profile/1").header("X-Auth-Token", "1023123123y12g31926391g2371623"))
                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"message\":\"" + Messages.UNAUTHORIZED_ACCESS.value() + "\"}"));
    }

    @Test
    public void testProfileInvalidSession () throws Exception {
        when(userService.findByToken(anyString())).thenReturn(user1);
        when(userService.isSessionValid(any(UserPojo.class))).thenReturn(false);
        mockMvc.perform(
                get("/users/profile/1").header("X-Auth-Token", "1023123123y12g31926391g2371623"))
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andExpect(content().json("{\"message\":\"" + Messages.UNAUTHORIZED_ACCESS.value() + "\"}"));
    }

}
