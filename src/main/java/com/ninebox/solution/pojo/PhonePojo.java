package com.ninebox.solution.pojo;

public class PhonePojo {

    private String ddd;
    private String number;

    public String getDdd () {
        return ddd;
    }

    public void setDdd (String ddd) {
        this.ddd = ddd;
    }

    public String getNumber () {
        return number;
    }

    public void setNumber (String number) {
        this.number = number;
    }

    @Override
    public String toString () {
        return "CreateUserPhone [ddd=" + ddd + ", number=" + number + "]";
    }

}
