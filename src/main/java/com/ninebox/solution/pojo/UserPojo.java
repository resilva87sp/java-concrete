package com.ninebox.solution.pojo;

import java.util.Date;
import java.util.List;

/**
 * @author renato
 */
public class UserPojo {

    private Long id;

    private String name;

    private String email;

    private String password;

    private List<PhonePojo> phones;

    private Date createdAt;

    private Date modifiedAt;

    private Date lastLoginAt;

    private String token;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public List<PhonePojo> getPhones () {
        return phones;
    }

    public void setPhones (List<PhonePojo> phones) {
        this.phones = phones;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt (Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt () {
        return modifiedAt;
    }

    public void setModifiedAt (Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getLastLoginAt () {
        return lastLoginAt;
    }

    public void setLastLoginAt (Date lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public String getToken () {
        return token;
    }

    public void setToken (String token) {
        this.token = token;
    }

    @Override
    public String toString () {
        return "UserPojo [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", phones="
                + phones + ", createdAt=" + createdAt + ", modifiedAt=" + modifiedAt + ", lastLoginAt=" + lastLoginAt
                + ", token=" + token + "]";
    }

}
