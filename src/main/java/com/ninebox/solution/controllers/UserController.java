package com.ninebox.solution.controllers;

import com.ninebox.solution.errors.custom.EmailAlreadyExistsException;
import com.ninebox.solution.errors.custom.InvalidLoginException;
import com.ninebox.solution.errors.custom.UnathorizedAccessException;
import com.ninebox.solution.pojo.LoginPojo;
import com.ninebox.solution.pojo.UserPojo;
import com.ninebox.solution.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@RestController
@RequestMapping("/users")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService service;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UserPojo> create (@RequestBody UserPojo userData) throws EmailAlreadyExistsException {
        LOG.info("Trying to create user with email {}", userData.getEmail());
        UserPojo savedUser = service.findByEmail(userData.getEmail());
        if (savedUser != null) {
            LOG.warn("User {} already exists", userData.getEmail());
            throw new EmailAlreadyExistsException();
        }
        UserPojo updatedUser = service.create(userData);
        LOG.info("Created user {}", updatedUser);
        return ResponseEntity.ok().body(updatedUser);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UserPojo> login (@RequestBody LoginPojo loginData) throws InvalidLoginException {
        LOG.info("Trying to login user {}", loginData);
        UserPojo user = service.findByEmail(loginData.getEmail());
        if (user == null) {
            LOG.warn("Email {} has not match in database", loginData.getEmail());
            throw new InvalidLoginException(HttpStatus.FORBIDDEN);
        }
        if (!service.isPasswordValid(loginData, user)) {
            LOG.warn("Requested password is not valid");
            throw new InvalidLoginException(HttpStatus.UNAUTHORIZED);
        }
        user.setLastLoginAt(new Date());
        service.save(user);
        LOG.info("Logged user {}", user.getEmail());
        return ResponseEntity.ok().body(user);
    }

    @RequestMapping(value = "/profile/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<UserPojo> profile (@PathVariable("id") Long id, HttpServletRequest request) throws UnathorizedAccessException {
        String token = request.getHeader("X-Auth-Token");
        LOG.info("Trying to get user profile from token {}", token);
        if (StringUtils.isEmpty(token)) {
            LOG.warn("Token is empty");
            throw new UnathorizedAccessException(HttpStatus.FORBIDDEN);
        }
        UserPojo user = service.findByToken(token);
        if (user == null) {
            LOG.warn("Token {} has not match in database", token);
            throw new UnathorizedAccessException(HttpStatus.FORBIDDEN);
        }
        if (!user.getId().equals(id)) {
            LOG.warn("Token {} found but ID {} has not matched", token, id);
            throw new UnathorizedAccessException(HttpStatus.FORBIDDEN);
        }
        if (!service.isSessionValid(user)) {
            LOG.warn("Session expired for token {}", token);
            throw new UnathorizedAccessException(HttpStatus.UNAUTHORIZED);
        }
        LOG.info("Returning profile for user {}", user.getEmail());
        return ResponseEntity.ok(user);
    }
}
