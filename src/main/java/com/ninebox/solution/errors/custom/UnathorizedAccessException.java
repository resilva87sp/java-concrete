package com.ninebox.solution.errors.custom;

import org.springframework.http.HttpStatus;

public class UnathorizedAccessException extends RuntimeException {

    private HttpStatus httpStatus;

    public UnathorizedAccessException (HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus () {
        return this.httpStatus;
    }
}
