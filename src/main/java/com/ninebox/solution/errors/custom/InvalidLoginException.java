package com.ninebox.solution.errors.custom;

import org.springframework.http.HttpStatus;

public class InvalidLoginException extends RuntimeException {

    private HttpStatus httpStatus;

    public InvalidLoginException (HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus () {
        return this.httpStatus;
    }
}
