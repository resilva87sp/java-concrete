package com.ninebox.solution.errors;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ninebox.solution.serializers.MessagesSerializer;

@JsonSerialize(using = MessagesSerializer.class)
public enum Messages {
    EMAIL_ALREADY_EXISTS("Email already exists."),
    INVALID_LOGIN("User and/or password are invalid."),
    UNAUTHORIZED_ACCESS("Not authorized.");

    private final String value;

    Messages (String value) {
        this.value = value;
    }

    public String value () {
        return this.value;
    }
}
