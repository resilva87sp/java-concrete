package com.ninebox.solution.errors;

import com.ninebox.solution.errors.custom.EmailAlreadyExistsException;
import com.ninebox.solution.errors.custom.InvalidLoginException;
import com.ninebox.solution.errors.custom.UnathorizedAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RESTExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EmailAlreadyExistsException.class})
    protected ResponseEntity<Object> handleEmailAlreadyExistsException (RuntimeException re, WebRequest request) {
        EmailAlreadyExistsException e = (EmailAlreadyExistsException) re;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return handleExceptionInternal(e, Messages.EMAIL_ALREADY_EXISTS, headers, HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler({InvalidLoginException.class, UnathorizedAccessException.class})
    protected ResponseEntity<Object> handleAccessExceptions (RuntimeException re, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        if (re.getClass().isAssignableFrom(InvalidLoginException.class)) {
            InvalidLoginException e = (InvalidLoginException) re;
            return handleExceptionInternal(e, Messages.INVALID_LOGIN, headers, e.getHttpStatus(), request);
        }
        UnathorizedAccessException e = (UnathorizedAccessException) re;
        return handleExceptionInternal(e, Messages.UNAUTHORIZED_ACCESS, headers, e.getHttpStatus(), request);
    }

}
