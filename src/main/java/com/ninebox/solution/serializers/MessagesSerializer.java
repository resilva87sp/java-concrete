package com.ninebox.solution.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.ninebox.solution.errors.Messages;

import java.io.IOException;

public class MessagesSerializer extends JsonSerializer<Messages> {

    @Override
    public void serialize (Messages messages, JsonGenerator generator,
                           SerializerProvider provider) throws IOException {

        generator.writeStartObject();
        generator.writeFieldName("message");
        generator.writeString(messages.value());
        generator.writeEndObject();
    }

}
