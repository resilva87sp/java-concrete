package com.ninebox.solution.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", length = 64, nullable = false)
    private String name;

    @Column(name = "email", length = 64, nullable = false)
    private String email;

    @Column(name = "password", length = 64, nullable = false)
    private String password;

    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @Column(name = "modified_at", nullable = true)
    private Date modifiedAt;

    @Column(name = "last_login_at", nullable = true)
    private Date lastLoginAt;

    @Column(name = "token", length = 1024, nullable = false)
    private String token;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt (Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt () {
        return modifiedAt;
    }

    public void setModifiedAt (Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getLastLoginAt () {
        return lastLoginAt;
    }

    public void setLastLoginAt (Date lastLoginAt) {
        this.lastLoginAt = lastLoginAt;
    }

    public String getToken () {
        return token;
    }

    public void setToken (String token) {
        this.token = token;
    }

    @Override
    public String toString () {
        return "UserEntity [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password
                + ", createdAt=" + createdAt + ", modifiedAt=" + modifiedAt + ", lastLoginAt=" + lastLoginAt
                + ", token=" + token + "]";
    }

}
