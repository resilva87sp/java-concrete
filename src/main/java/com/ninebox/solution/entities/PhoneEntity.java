package com.ninebox.solution.entities;

import javax.persistence.*;

@Entity
@Table(name = "phones")
public class PhoneEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "ddd", nullable = false)
    private String ddd;

    @Column(name = "number", nullable = false)
    private String number;

    @ManyToOne
    private UserEntity userEntity;

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getDdd () {
        return ddd;
    }

    public void setDdd (String ddd) {
        this.ddd = ddd;
    }

    public String getNumber () {
        return number;
    }

    public void setNumber (String number) {
        this.number = number;
    }

    public UserEntity getUserEntity () {
        return userEntity;
    }

    public void setUserEntity (UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @Override
    public String toString () {
        return "PhoneEntity{" +
                "id=" + id +
                ", ddd='" + ddd + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
