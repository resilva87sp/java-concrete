package com.ninebox.solution.services;

import com.ninebox.solution.entities.PhoneEntity;
import com.ninebox.solution.entities.UserEntity;
import com.ninebox.solution.pojo.LoginPojo;
import com.ninebox.solution.pojo.PhonePojo;
import com.ninebox.solution.pojo.UserPojo;
import com.ninebox.solution.repositories.PhoneRepository;
import com.ninebox.solution.repositories.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private static final String LAST_LOGIN_TIMEOUT_PROP = "last.login.timeout";

    private StrongPasswordEncryptor passwordEncryptor;

    @Autowired
    private KeystoreReader encryptionReader;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init () {
        passwordEncryptor = new StrongPasswordEncryptor();
    }

    public UserPojo create (UserPojo user) {
        Date now = new Date();
        user.setCreatedAt(now);
        user.setLastLoginAt(now);
        user.setModifiedAt(now);
        user.setPassword(passwordEncryptor.encryptPassword(user.getPassword()));
        user.setToken(getJWT(user));
        UserEntity entity = userPojo2Entity(user);
        entity = userRepository.save(entity);
        List<PhoneEntity> phones = phonePojo2Entities(user, entity);
        phoneRepository.save(phones);
        user.setId(entity.getId());
        return user;
    }

    public UserPojo save (UserPojo user) {
        UserEntity entity = userPojo2Entity(user);
        entity = userRepository.save(entity);
        return userEntity2Pojo(entity);
    }

    public boolean isPasswordValid (LoginPojo loginData, UserPojo user) {
        return passwordEncryptor.checkPassword(loginData.getPassword(), user.getPassword());
    }

    public boolean isSessionValid (UserPojo user) {
        Date now = new Date();
        Date lastLogin = user.getLastLoginAt();
        if (lastLogin == null) {
            return false;
        }
        long diff = now.getTime() - lastLogin.getTime();
        long diffMinutes = diff / (60 * 1000) % 60;
        Integer timeout = environment.getProperty(LAST_LOGIN_TIMEOUT_PROP, Integer.class, 30);
        return diffMinutes < timeout;

    }

    public UserPojo findByEmail (String email) {
        UserEntity entity = userRepository.findByEmail(email);
        return loadEntites(entity);
    }

    public UserPojo findByToken (String token) {
        UserEntity entity = userRepository.findByToken(token);
        return loadEntites(entity);
    }

    private UserPojo loadEntites (UserEntity userEntity) {
        UserPojo pojo = null;
        if (userEntity != null) {
            pojo = userEntity2Pojo(userEntity);
            List<PhoneEntity> phoneEntities = phoneRepository.findByUserEntity(userEntity);
            pojo.setPhones(phoneEntities2Pojo(phoneEntities));
        }
        return pojo;
    }

    private UserEntity userPojo2Entity (UserPojo userPojo) {
        UserEntity entity = new UserEntity();
        entity.setId(userPojo.getId());
        entity.setCreatedAt(userPojo.getCreatedAt());
        entity.setLastLoginAt(userPojo.getLastLoginAt());
        entity.setModifiedAt(userPojo.getModifiedAt());
        entity.setEmail(userPojo.getEmail());
        entity.setName(userPojo.getName());
        entity.setPassword(userPojo.getPassword());
        entity.setToken(userPojo.getToken());
        return entity;
    }

    private List<PhoneEntity> phonePojo2Entities (UserPojo userPojo, UserEntity userEntity) {
        List<PhonePojo> phonesPojo = userPojo.getPhones();
        List<PhoneEntity> result = new ArrayList<>();
        if (phonesPojo != null && !phonesPojo.isEmpty()) {
            for (PhonePojo phonePojo : phonesPojo) {
                PhoneEntity entity = new PhoneEntity();
                entity.setDdd(phonePojo.getDdd());
                entity.setNumber(phonePojo.getNumber());
                entity.setUserEntity(userEntity);
                result.add(entity);
            }
        }
        return result;
    }

    private List<PhonePojo> phoneEntities2Pojo (List<PhoneEntity> phoneEntities) {
        if (phoneEntities == null || phoneEntities.isEmpty()) {
            return Collections.emptyList();
        }
        List<PhonePojo> result = new ArrayList<>();
        for (PhoneEntity entity : phoneEntities) {
            PhonePojo phonePojo = new PhonePojo();
            phonePojo.setNumber(entity.getNumber());
            phonePojo.setDdd(entity.getDdd());
            result.add(phonePojo);
        }
        return result;
    }

    private UserPojo userEntity2Pojo (UserEntity entity) {
        UserPojo pojo = new UserPojo();
        pojo.setCreatedAt(entity.getCreatedAt());
        pojo.setEmail(entity.getEmail());
        pojo.setId(entity.getId());
        pojo.setLastLoginAt(entity.getLastLoginAt());
        pojo.setModifiedAt(entity.getModifiedAt());
        pojo.setName(entity.getName());
        pojo.setPassword(entity.getPassword());
        pojo.setToken(entity.getToken());
        return pojo;
    }

    private String getJWT (UserPojo pojo) {
        try {
            String jwtToken = Jwts.builder().setSubject(pojo.getEmail())
                    .signWith(SignatureAlgorithm.HS512, encryptionReader.getKey()).compact();
            LOG.info("JWT token created: {}", jwtToken);
            return jwtToken;
        } catch (Exception e) {
            LOG.error("Could not generate JWT token: ", e);
        }
        return null;
    }

}
