package com.ninebox.solution.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;

@Service
public class KeystoreReader {

    private static final Logger LOG = LoggerFactory.getLogger(KeystoreReader.class);
    private static final String KEYSTORE_PASSWORD_PROP = "keystore.password";

    private Key key;

    @Autowired
    private Environment env;

    @PostConstruct
    public void init () throws Exception {
        readKey("/keystore");
    }

    public Key getKey () {
        return key;
    }

    private void readKey (String filename) throws Exception {
        InputStream is = this.getClass().getResourceAsStream(filename);
        KeyStore ks = KeyStore.getInstance("JCEKS");
        String keysStorePassword = env.getProperty(KEYSTORE_PASSWORD_PROP);
        char[] password = keysStorePassword.toCharArray();
        ks.load(is, password);
        KeyStore.ProtectionParameter protection = new KeyStore.PasswordProtection(password);
        KeyStore.SecretKeyEntry entry = (KeyStore.SecretKeyEntry) ks.getEntry("jwt-key", protection);
        this.key = entry.getSecretKey();
    }

}
