package com.ninebox.solution.repositories;


import com.ninebox.solution.entities.PhoneEntity;
import com.ninebox.solution.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhoneRepository extends CrudRepository<PhoneEntity, Long> {
    List<PhoneEntity> findByUserEntity (UserEntity userEntity);
}
