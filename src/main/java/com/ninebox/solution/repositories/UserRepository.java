package com.ninebox.solution.repositories;

import com.ninebox.solution.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByEmail (String email);

    UserEntity findByToken (String token);
}
